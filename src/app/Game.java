package app;

public class Game
{
    private final int NUM_ROWS = 8;
    private final int NUM_COLUMS = 8;

    private byte[][] gameBoard;
    private int[] stackSizes;
    private int lastPlacedColumn;
    private byte lastPlayer;

    public Game()
    {
        reset();
    }

    public void reset()
    {
        gameBoard = new byte[NUM_COLUMS][NUM_ROWS];
        stackSizes = new int[NUM_COLUMS];

        // playField = new byte[][] {{1,0,0,0,0,0},
        //                           {2,0,0,0,0,0},
        //                           {1,2,1,0,0,0},
        //                           {2,1,2,1,0,0},
        //                           {0,0,0,0,0,0},
        //                           {0,0,0,0,0,0},
        //                           {0,0,0,0,0,0}};
        // 
        // stackSizes = new int[] {1,1,3,4,0,0,0};

        // playField = new byte[][] {
        //                         {2,1,2,1,0,0},
        //                         {1,2,1,0,0,0},
        //                         {2,0,0,0,0,0},
        //                         {1,0,0,0,0,0},
        //                         {0,0,0,0,0,0},
        //                         {0,0,0,0,0,0},
        //                         {0,0,0,0,0,0}
        //                     };
        // 
        // stackSizes = new int[] {4,3,1,1,0,0,0};
    }

    public boolean putGamePiece(int column, byte player)
    {
        if (column >= NUM_COLUMS)
        {
            // Error: Row out of bounds
            return false;
        }

        int currentStackSize = stackSizes[column];
        if (currentStackSize >= NUM_ROWS)
        {
            // Error: Column already full
            return false;
        }

        stackSizes[column]++;
        gameBoard[column][currentStackSize] = player;

        lastPlacedColumn = column;
        lastPlayer = player;

        return true;
    }

    public boolean checkWin()
    {
        return checkVerticalWin() || checkHorizontalWin() || checkDiagonalWin(false) || checkDiagonalWin(true);
    }

    private boolean checkVerticalWin()
    {
        int stackSize = stackSizes[lastPlacedColumn];

        if (stackSize >= 4)
        {
            // Iterate stack from top to bottom and check if  all pieces are the current player's
            for (int iPiece = stackSize - 1; iPiece >= stackSize - 4; iPiece--)
            {
                if (gameBoard[lastPlacedColumn][iPiece] != lastPlayer)
                {
                    return false;
                }
            }

            // All pieces in the stack were placed by the last player
            return true;
        }

        // Stack is too low
        return false;
    }

    private boolean checkHorizontalWin()
    {
        // Start with 1 because the player just placed one
        int nConnectedPieces = 1;
        int lastPlacedStackSize = stackSizes[lastPlacedColumn];

        // Start with the left
        if (lastPlacedColumn > 0)
        {
            for (int nextColumn = lastPlacedColumn - 1; nextColumn >= 0; nextColumn--)
            {
                // Stop if colum hasn't reached the same height yet
                if (stackSizes[nextColumn] < lastPlacedStackSize)
                {
                    break;
                }

                // Stop if piece belongs to other player
                if (gameBoard[nextColumn][lastPlacedStackSize - 1] != lastPlayer)
                {
                    break;
                }
                
                nConnectedPieces++;
            }
        }

        // Continue with the right
        if (lastPlacedColumn < gameBoard.length - 1)
        {
            for (int nextColumn = lastPlacedColumn + 1; nextColumn <= gameBoard.length - 1; nextColumn++)
            {
                // Stop if colum hasn't reached the same height yet
                if (stackSizes[nextColumn] < lastPlacedStackSize)
                {
                    break;
                }

                // Stop if piece belongs to other player
                if (gameBoard[nextColumn][lastPlacedStackSize - 1] != lastPlayer)
                {
                    break;
                }
                
                nConnectedPieces++;
            }
        }

        return nConnectedPieces >= 4;
    }

    private boolean checkDiagonalWin(boolean mirrored)
    {
        // Start with one because the player just placed one
        int nConnectedPieces = 1;

        // Add all pieces from the bottom left diagonal
        if (lastPlacedColumn > 0)
        {
            int nextRow = stackSizes[getMirroredIndex(lastPlacedColumn, stackSizes.length, mirrored)] - 1;

            for (int nextColumn = lastPlacedColumn - 1; nextColumn >= 0; nextColumn--)
            {
                // move down one row
                nextRow--;
                
                // Stop if out of bounds
                if (nextRow < 0)
                {
                    break;
                }

                // Stop if piece belongs to other player
                if (gameBoard[getMirroredIndex(nextColumn, gameBoard.length, mirrored)][nextRow] != lastPlayer)
                {
                    break;
                }

                nConnectedPieces++;
            }
        }

        // Add all pieces from the top right diagonal
        if (lastPlacedColumn < gameBoard.length - 1)
        {
            int nextRow = stackSizes[getMirroredIndex(lastPlacedColumn, stackSizes.length, mirrored)] - 1;

            for (int nextColumn = lastPlacedColumn + 1; nextColumn <= gameBoard.length - 1; nextColumn++)
            {
                // move up one row
                nextRow++;

                // Stop if colum hasn't reached the required height yet
                if (nextRow > stackSizes[getMirroredIndex(nextColumn, stackSizes.length, mirrored)] - 1)
                {
                    break;
                }

                // Stop if piece belongs to other player
                if (gameBoard[getMirroredIndex(nextColumn, gameBoard.length, mirrored)][nextRow] != lastPlayer)
                {
                    break;
                }

                nConnectedPieces++;
            }
        }

        return nConnectedPieces >= 4;
    }

    boolean isBoardFull()
    {
        for (int i = 0; i < stackSizes.length; i++)
        {
            if (stackSizes[i] < NUM_ROWS)
            {
                return false;
            }
        }

        return true;
    }

    private int getMirroredIndex(int index, int arrayLength, boolean mirrored)
    {
        if (!mirrored)
        {
            return index;
        }

        return arrayLength - 1 - index;
    }

    public byte[][] getGameBoard()
    {
        return gameBoard;
    }
}