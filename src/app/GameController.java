package app;

public class GameController
{
    private Game game;
    private IPlayer player1;
    private IPlayer player2;
    private byte currentPlayer;

    public GameController(Game game, IPlayer player1, IPlayer player2)
    {
        this.game = game;
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = 2;
    }

    public void startGame()
    {
        System.out.println("Starting new game.");
        runGameLoop();
    }
    
    private void runGameLoop()
    {
        boolean playerWon = false;
        boolean boardFull = false;

        outputGame();

        do
        {
            IPlayer player = getNextPlayer();
            System.out.println("Your move, Player " + currentPlayer);

            boolean validPiecePlaced = false;

            do 
            {
                int column = player.getNextMove();
                
                if (game.putGamePiece(column, currentPlayer) == false)
                {
                    System.out.println("Invalid column, try again.");
                }
                else {
                    validPiecePlaced = true;
                }
            } while (validPiecePlaced == false);

            outputGame();

            playerWon = game.checkWin();
            boardFull = game.isBoardFull();
        } 
        while (!playerWon && !boardFull);

        if (playerWon)
        {
            announceWinner();
        } else 
        {
            System.out.println("That's a tie.");
        }
    }

    private void announceWinner()
    {
        System.out.println("Congratulations, Player " + currentPlayer + " [" + getCharRepresentation(currentPlayer) + "], you truly are the smartest one!");
    }

    private void outputGame()
    {
        byte[][] gameBoard = game.getGameBoard();

        for (int row = gameBoard[0].length - 1; row >= 0; row--)
        {
            for (int column = 0; column < gameBoard.length; column++)
            {
                System.out.print(getCharRepresentation(gameBoard[column][row]));
            }

            System.out.println();
        }

        for (int i = 0; i < gameBoard.length; i++)
        {
            System.out.print(i+1);
        }

        System.out.println();
    }

    private char getCharRepresentation(int i)
    {
        switch(i)
        {
            case 1:
                return 'X';
            case 2:
                return 'O';
            case 0:
                return '.';
            default:
                return '?';
        }
    }

    private IPlayer getNextPlayer()
    {
        if (currentPlayer == 1)
        {
            currentPlayer = 2;
            return player2;
        }
        else 
        {
            currentPlayer = 1;
            return player1;
        }
    }
}