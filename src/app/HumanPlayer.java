package app;

import java.io.IOException;

class HumanPlayer implements IPlayer
{
    @Override
    public int getNextMove() {
        try 
        {
            while (true)
            {   
                char input = (char)System.in.read();
                flushReadInput();
                
                if (input >= '1' && input <= '9')
                {
                    return input - '1';
                }
                else {
                    System.out.println("Invalid input [" + input + "]");
                }
            }
        } 
        catch (IOException e) 
        {
			e.printStackTrace();
        }
        
        return 0;
    }

    private void flushReadInput() throws IOException
    {
        while (System.in.available() > 0 && System.in.read() != -1);
    }

}