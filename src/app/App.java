package app;

public class App {
    public static void main(String[] args) throws Exception {
        Game g = new Game();
        IPlayer p1 = new HumanPlayer();
        IPlayer p2 = new HumanPlayer();

        GameController controller = new GameController(g, p1, p2);
        controller.startGame();
    }
}